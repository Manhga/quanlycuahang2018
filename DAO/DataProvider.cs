﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace DAO
{
    public class DataProvider
    {
        private static DataProvider instance; // Ctrl + R + E

        public static DataProvider Instance
        {
            get { if (instance == null) instance = new DataProvider(); return DataProvider.instance; }
            private set { DataProvider.instance = value; }
        }

        private String Connect = @"Data Source=DESKTOP-VL7A4CB;Initial Catalog=QuanLyCuaHang2018;Integrated Security=True";

        public string GetValueFunction(string query)
        {
            using (SqlConnection conn = new SqlConnection(Connect))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    return cmd.ExecuteScalar().ToString();
                }
            }
        }
        public DataTable ExecuteQuery(String query, object[] parameter =null)
        {
            DataTable data = new DataTable();
            using (SqlConnection connection = new SqlConnection(Connect))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);


                if (parameter != null)
                {
                    string[] listPar = query.Split(' ');

                    int index = 0;

                    foreach (string item in listPar)
                    {
                        if (item.Contains("@"))
                        {
                            command.Parameters.AddWithValue(item, parameter[index]);
                            index++;
                        }
                    }
                }
                SqlDataAdapter adapter = new SqlDataAdapter(command);

                adapter.Fill(data);

                connection.Close();
            }
            return data;
        }

        public int ExecuteNonQuery(string query, object[] parameter = null)
        {
            int result = -1;
            using (SqlConnection connection = new SqlConnection(Connect))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);

                if (parameter != null)
                {
                    string[] listPar = query.Split(' ');

                    int index = 0;

                    foreach (string item in listPar)
                    {
                        if (item.Contains('@'))
                        {
                            command.Parameters.AddWithValue(item, parameter[index]);
                            index++;
                        }
                    }
                }
                try
                {
                    result = command.ExecuteNonQuery();
                }
                catch {
                   
                }
                connection.Close();
            }
            return result;
        }


        public object ExecuteScalar(string query, object[] parameter = null)
        {
            object data = 0;

            using (SqlConnection connection = new SqlConnection(Connect))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);

                if (parameter != null)
                {
                    string[] listPara = query.Split(' ');
                    int i = 0;
                    foreach (string item in listPara)
                    {
                        if (item.Contains('@'))
                        {
                            command.Parameters.AddWithValue(item, parameter[i]);
                            i++;
                        }
                    }
                }

                data = command.ExecuteScalar();

                connection.Close();
            }

            return data;
        }
    }
}
