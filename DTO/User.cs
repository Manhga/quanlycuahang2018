﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class User
    {
        public int manv { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public int phone { get; set; }
        public string password { get; set; }
        public int role_id { get; set; }
        public string sex { get; set; }
    }
}
