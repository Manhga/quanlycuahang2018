﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DTO;

namespace BUS
{
    public class managerBUS
    {
        private static managerBUS instance;

        public static managerBUS Instance
        {
            get { if (instance == null) instance = new managerBUS(); return instance; }
            set { instance = value; }
        }

        public DataTable SelectListEmployee()
        {
            String query = @"select [manv]
      ,[name]
      ,[email]
      ,[phone]
      ,[role_id]
      from users";
            return DataProvider.Instance.ExecuteQuery(query);
        }

        public User SelectInfoEmloyee(int idnv)
        {
            String query = @"select [manv]
      ,[name]
      ,[email]
      ,[phone]
      ,[password]
      ,[role_id]
      from users where manv = "+ idnv ;
            DataTable dt = DataProvider.Instance.ExecuteQuery(query);
            User employee = new User();
            employee.manv = int.Parse(dt.Rows[0][0].ToString());
            employee.name = dt.Rows[0][1].ToString();
            employee.email = dt.Rows[0][2].ToString();
            employee.phone = int.Parse(dt.Rows[0][3].ToString());
            employee.password = dt.Rows[0][4].ToString();
            employee.role_id = int.Parse(dt.Rows[0][5].ToString());
            return employee;
        }

        public int CheckLogin(int taikhoan, String password, out User employee)
        {
            string query = "F_Login @taikhoan";
            DataTable temp = DataProvider.Instance.ExecuteQuery(query, new object[] { taikhoan});
            if (temp.Rows.Count > 0)
            {
                int manv = int.Parse(temp.Rows[0][0].ToString());
                User nv = SelectInfoEmloyee(manv);
                if (nv.password.Equals(password))
                {
                    employee = nv;
                    return 1;
                }
                else
                {
                    employee = null;
                    return 0;
                }
            }
            else
            {
                employee = null;
                return -1;
            }
        }

        public DataTable ListEmployee()
        {
            String query = @"select [manv]
      ,[name]
      ,[role]
      from users, roles where users.role_id = roles.role_id";
            return DataProvider.Instance.ExecuteQuery(query);
        }

        public int AddEmployee(User user)
        {
            string query = @"Emp_Insert @manv , @name , @email , @phone , @password , @role_id , @sex ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { user.manv, user.name, user.email, user.phone, user.password, user.role_id, user.sex });
        }

        public int DelEmployee(User user)
        {
            string query = @"Emp_Del @manv";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { user.manv });
        }
    }
}
