﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAO;
using DTO;
namespace BUS
{
    public class Product_BUS
    {
        private Product_BUS instance;

        internal Product_BUS Instance
        {

            get
            {
                if (instance == null)
                    instance = new Product_BUS();
                return instance;
            }
        }
        public DataTable Product_Load()
        {
            string query = "select [product] ,[category_name] ,[cost] ,[total] ,[Sold] from products inner join categories on products.category_id= categories.category_id";
            return DataProvider.Instance.ExecuteQuery(query);
        }
        public DataTable Product_Load(string tendia)
        {
            string query = "select [product] ,[category_name] ,[cost] ,[total] ,[Sold] from products inner join categories on products.category_id= categories.category_id where product  like n'%" + tendia +"%' ";
            return DataProvider.Instance.ExecuteQuery(query, new object[] { tendia });
        }
        public DataTable LayDanhSachDiaTheoTheLoai(string tentl)
        {
            string query = "select [product] ,[category_name] ,[cost] ,[total] ,[Sold] from products inner join categories on products.category_id= categories.category_id where category_name like n'%" + tentl + "%'";
            return DataProvider.Instance.ExecuteQuery(query);
        }
        public int ThemDia(string tendia, int matl, int gia, int soluong, int daban)
        {
            string query = "insert into products values (N'" + tendia + "','" + matl + "','" + gia + "','" + soluong + "','" + daban + "')";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { tendia, matl, gia, soluong, daban });
        }
        public int SuaDia(string tendia, int matl, int gia, int soluong, int daban)
        {
            string query = "update products set product = N'" + tendia + "', category_id ='" + matl + "', cost='" + gia + "',total='" + soluong + "',Sold='" + daban + "'";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { tendia, matl, gia, soluong, daban });
        }
        public int XoaDia(string tendia)
        {
            string query = "delete products where product= '" + tendia + "'";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { tendia });
        }
    }
}
