﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DTO;
using System.Windows.Forms;
using System.Data;

namespace BUS
{
    public class CategoryBUS
    {
        private CategoryBUS instance;

        internal CategoryBUS Instance
        {

            get
            {
                if (instance == null)
                    instance = new CategoryBUS();
                return instance;
            }
        }
        public DataTable cate_Load()
        {
            string query = "select [category_id] ,[category_name] ,[created_at] from categories";
            return DataProvider.Instance.ExecuteQuery(query);
        }
        public DataTable categories_Load(string matl)
        {
            string query = "select [category_name] ,[created_at] from categoies where category_id  like '%' + @par1 +'%' or category like '%' + @par1 +'%' ";
            return DataProvider.Instance.ExecuteQuery(query, new object[] { matl, matl });
        }
        public List<Category> LayDanhSachTheLoai_Search(string matl)
        {
            List<Category> list = new List<Category>();
            DataTable dt = categories_Load(matl);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string MaGV = dt.Rows[i][0].ToString();
                    list.Add(LayThongTinTheLoai(matl));
                }
            }
            return list;
        }
        public Category LayThongTinTheLoai(string matheloai)
        {
            string query = "select [category_name] ,[created_at] from categories where category_id ="+matheloai;
            DataTable dt = DataProvider.Instance.ExecuteQuery(query, new object[] { matheloai });
            Category cate = new Category();
            cate.Id = int.Parse(dt.Rows[0][0].ToString());
            cate.Name = dt.Rows[0][1].ToString();
            cate.Date = DateTime.Parse(dt.Rows[0][2].ToString());
            return cate;
        }

        public bool KiemTraTonTai(string tentl)
        {
            string query = "Select * from categories where category_name ='"+tentl+"' "  ;
            DataTable dt = DataProvider.Instance.ExecuteQuery(query, new object[] { tentl });
            if (dt.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        
        public int ThemTheLoai(Category category)
        {
            /*'n"+tentl+"', '"+date+"'*/
            string query = @"INSERT INTO [dbo].[categories]
           ([category_name]
           ,[created_at])
     VALUES
           ( @name 
           , @date )";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { category.Name, category.Date });
        }

        public int SuaTheLoai(Category category)
        {
            string query = @"Cate_Update @matl , @tentl ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { category.Id , category.Name });
        }

        public int XoaTheLoai(Category category)
        {
            string query = @"Cate_Dell @tentl";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { category.Name });
        }

        public DataTable category_load()
        {
            string query = "select [category_id] ,[category_name] from categories";
            return DataProvider.Instance.ExecuteQuery(query);
        }
    }
}
