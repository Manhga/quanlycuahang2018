﻿namespace QuanLyCuaHang2018
{
    partial class listEmployee
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.grdListEmployee = new DevExpress.XtraGrid.GridControl();
            this.gvListEmployee = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grdCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdChucVu = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdListEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListEmployee)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.btnDelete);
            this.panelControl2.Controls.Add(this.btnSave);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(657, 100);
            this.panelControl2.TabIndex = 1;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(526, 38);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(98, 32);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(380, 38);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(102, 32);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Lưu";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.grdListEmployee);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 100);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(657, 239);
            this.panelControl1.TabIndex = 2;
            // 
            // grdListEmployee
            // 
            this.grdListEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdListEmployee.Location = new System.Drawing.Point(2, 2);
            this.grdListEmployee.MainView = this.gvListEmployee;
            this.grdListEmployee.Name = "grdListEmployee";
            this.grdListEmployee.Size = new System.Drawing.Size(653, 235);
            this.grdListEmployee.TabIndex = 2;
            this.grdListEmployee.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvListEmployee});
            // 
            // gvListEmployee
            // 
            this.gvListEmployee.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grdCode,
            this.grdName,
            this.grdChucVu});
            this.gvListEmployee.GridControl = this.grdListEmployee;
            this.gvListEmployee.Name = "gvListEmployee";
            // 
            // grdCode
            // 
            this.grdCode.Caption = "Mã nhân viên";
            this.grdCode.FieldName = "manv";
            this.grdCode.Name = "grdCode";
            this.grdCode.Visible = true;
            this.grdCode.VisibleIndex = 1;
            // 
            // grdName
            // 
            this.grdName.Caption = "Tên Nhân viên";
            this.grdName.FieldName = "name";
            this.grdName.Name = "grdName";
            this.grdName.Visible = true;
            this.grdName.VisibleIndex = 0;
            // 
            // grdChucVu
            // 
            this.grdChucVu.Caption = "Chức vụ";
            this.grdChucVu.FieldName = "role";
            this.grdChucVu.Name = "grdChucVu";
            this.grdChucVu.Visible = true;
            this.grdChucVu.VisibleIndex = 2;
            // 
            // listEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.Name = "listEmployee";
            this.Size = new System.Drawing.Size(657, 339);
            this.Load += new System.EventHandler(this.listEmployee_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdListEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListEmployee)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl grdListEmployee;
        private DevExpress.XtraGrid.Views.Grid.GridView gvListEmployee;
        private DevExpress.XtraGrid.Columns.GridColumn grdCode;
        private DevExpress.XtraGrid.Columns.GridColumn grdName;
        private DevExpress.XtraGrid.Columns.GridColumn grdChucVu;
    }
}
