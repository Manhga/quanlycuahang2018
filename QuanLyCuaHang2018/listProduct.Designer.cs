﻿namespace QuanLyCuaHang2018
{
    partial class listProduct
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.grdListProduct = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grdcName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdcCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdcDate_input = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdcTotal_Input = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdcTotal_Browed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdcCost = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdListProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.grdListProduct);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(805, 501);
            this.panelControl1.TabIndex = 0;
            // 
            // grdListProduct
            // 
            this.grdListProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdListProduct.Location = new System.Drawing.Point(2, 2);
            this.grdListProduct.MainView = this.gridView1;
            this.grdListProduct.Name = "grdListProduct";
            this.grdListProduct.Size = new System.Drawing.Size(801, 497);
            this.grdListProduct.TabIndex = 0;
            this.grdListProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grdcName,
            this.grdcCategory,
            this.grdcDate_input,
            this.grdcTotal_Input,
            this.grdcTotal_Browed,
            this.grdcCost});
            this.gridView1.GridControl = this.grdListProduct;
            this.gridView1.Name = "gridView1";
            // 
            // grdcName
            // 
            this.grdcName.Caption = "Tên hàng hóa";
            this.grdcName.Name = "grdcName";
            this.grdcName.Visible = true;
            this.grdcName.VisibleIndex = 0;
            // 
            // grdcCategory
            // 
            this.grdcCategory.Caption = "Thể loại";
            this.grdcCategory.Name = "grdcCategory";
            this.grdcCategory.Visible = true;
            this.grdcCategory.VisibleIndex = 1;
            // 
            // grdcDate_input
            // 
            this.grdcDate_input.Caption = "Ngày nhập";
            this.grdcDate_input.Name = "grdcDate_input";
            this.grdcDate_input.Visible = true;
            this.grdcDate_input.VisibleIndex = 2;
            // 
            // grdcTotal_Input
            // 
            this.grdcTotal_Input.Caption = "Số lượng nhập";
            this.grdcTotal_Input.Name = "grdcTotal_Input";
            this.grdcTotal_Input.Visible = true;
            this.grdcTotal_Input.VisibleIndex = 3;
            // 
            // grdcTotal_Browed
            // 
            this.grdcTotal_Browed.Caption = "Số lượng còn";
            this.grdcTotal_Browed.Name = "grdcTotal_Browed";
            this.grdcTotal_Browed.Visible = true;
            this.grdcTotal_Browed.VisibleIndex = 4;
            // 
            // grdcCost
            // 
            this.grdcCost.Caption = "Đơn giá";
            this.grdcCost.Name = "grdcCost";
            this.grdcCost.Visible = true;
            this.grdcCost.VisibleIndex = 5;
            // 
            // listProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl1);
            this.Name = "listProduct";
            this.Size = new System.Drawing.Size(805, 501);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdListProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl grdListProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn grdcName;
        private DevExpress.XtraGrid.Columns.GridColumn grdcCategory;
        private DevExpress.XtraGrid.Columns.GridColumn grdcDate_input;
        private DevExpress.XtraGrid.Columns.GridColumn grdcTotal_Input;
        private DevExpress.XtraGrid.Columns.GridColumn grdcTotal_Browed;
        private DevExpress.XtraGrid.Columns.GridColumn grdcCost;
    }
}
