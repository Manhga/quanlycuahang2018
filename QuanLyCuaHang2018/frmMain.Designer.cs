﻿namespace QuanLyCuaHang2018
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnListEmployee = new DevExpress.XtraBars.BarButtonItem();
            this.btnAddEmployee = new DevExpress.XtraBars.BarButtonItem();
            this.btnListProduct = new DevExpress.XtraBars.BarButtonItem();
            this.btnInsertProduct = new DevExpress.XtraBars.BarButtonItem();
            this.btnListProductSend = new DevExpress.XtraBars.BarButtonItem();
            this.btnListCategory = new DevExpress.XtraBars.BarButtonItem();
            this.btnInsertCategory = new DevExpress.XtraBars.BarButtonItem();
            this.btnList = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.paCHienThi = new DevExpress.XtraEditors.PanelControl();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paCHienThi)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnListEmployee,
            this.btnAddEmployee,
            this.btnListProduct,
            this.btnInsertProduct,
            this.btnListProductSend,
            this.btnListCategory,
            this.btnInsertCategory,
            this.btnList});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 9;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage2});
            this.ribbon.Size = new System.Drawing.Size(885, 143);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // btnListEmployee
            // 
            this.btnListEmployee.Caption = "Nhân viên";
            this.btnListEmployee.Id = 1;
            this.btnListEmployee.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnListEmployee.ImageOptions.Image")));
            this.btnListEmployee.Name = "btnListEmployee";
            this.btnListEmployee.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btnListEmployee.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnListEmployee_ItemClick);
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.Caption = "Thêm nhân viên";
            this.btnAddEmployee.Id = 2;
            this.btnAddEmployee.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddEmployee.ImageOptions.Image")));
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAddEmployee_ItemClick);
            // 
            // btnListProduct
            // 
            this.btnListProduct.Caption = "Danh sách hàng hóa";
            this.btnListProduct.Id = 3;
            this.btnListProduct.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnListProduct.ImageOptions.Image")));
            this.btnListProduct.Name = "btnListProduct";
            this.btnListProduct.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btnListProduct.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnListProduct_ItemClick);
            // 
            // btnInsertProduct
            // 
            this.btnInsertProduct.Caption = "Thêm hàng hóa";
            this.btnInsertProduct.Id = 4;
            this.btnInsertProduct.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnInsertProduct.ImageOptions.Image")));
            this.btnInsertProduct.Name = "btnInsertProduct";
            this.btnInsertProduct.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInsertProduct_ItemClick);
            // 
            // btnListProductSend
            // 
            this.btnListProductSend.Caption = "Hàng hóa đã bán";
            this.btnListProductSend.Id = 5;
            this.btnListProductSend.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnListProductSend.ImageOptions.Image")));
            this.btnListProductSend.Name = "btnListProductSend";
            // 
            // btnListCategory
            // 
            this.btnListCategory.Caption = "Danh sách thể loại";
            this.btnListCategory.Id = 6;
            this.btnListCategory.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnListCategory.ImageOptions.Image")));
            this.btnListCategory.Name = "btnListCategory";
            this.btnListCategory.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btnListCategory.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnListCategory_ItemClick);
            // 
            // btnInsertCategory
            // 
            this.btnInsertCategory.Caption = "Thêm thể loại";
            this.btnInsertCategory.Id = 7;
            this.btnInsertCategory.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnInsertCategory.ImageOptions.Image")));
            this.btnInsertCategory.Name = "btnInsertCategory";
            this.btnInsertCategory.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInsertCategory_ItemClick);
            // 
            // btnList
            // 
            this.btnList.Caption = "Thống kê hóa đơn";
            this.btnList.Id = 8;
            this.btnList.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnList.ImageOptions.Image")));
            this.btnList.Name = "btnList";
            this.btnList.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Hệ Thống";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnListEmployee);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnAddEmployee);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Thống kê";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup3,
            this.ribbonPageGroup4});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Tác vụ Manager";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnListProduct);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnInsertProduct);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnListProductSend);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Hàng hóa";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnListCategory);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnInsertCategory);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Thể loại";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.btnList);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Hóa đơn";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 525);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(885, 31);
            // 
            // paCHienThi
            // 
            this.paCHienThi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paCHienThi.Location = new System.Drawing.Point(0, 143);
            this.paCHienThi.Name = "paCHienThi";
            this.paCHienThi.Size = new System.Drawing.Size(885, 382);
            this.paCHienThi.TabIndex = 2;
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "ribbonPage3";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 556);
            this.Controls.Add(this.paCHienThi);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Name = "frmMain";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Hệ thống quản lý của hàng băng đĩa";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paCHienThi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem btnListEmployee;
        private DevExpress.XtraEditors.PanelControl paCHienThi;
        private DevExpress.XtraBars.BarButtonItem btnAddEmployee;
        private DevExpress.XtraBars.BarButtonItem btnListProduct;
        private DevExpress.XtraBars.BarButtonItem btnInsertProduct;
        private DevExpress.XtraBars.BarButtonItem btnListProductSend;
        private DevExpress.XtraBars.BarButtonItem btnListCategory;
        private DevExpress.XtraBars.BarButtonItem btnInsertCategory;
        private DevExpress.XtraBars.BarButtonItem btnList;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
    }
}