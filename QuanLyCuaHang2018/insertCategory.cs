﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO;

namespace QuanLyCuaHang2018
{
    public partial class insertCategory : DevExpress.XtraEditors.XtraUserControl
    {
        public insertCategory()
        {
            InitializeComponent();
        }

        private void panelControl2_Paint(object sender, PaintEventArgs e)
        {
           
        }

        listCategory list = new listCategory();

        private void insertCategory_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        public void LoadData()
        {
            panelControl2.Controls.Clear();
            listCategory list = new listCategory();
            list.Dock = DockStyle.Fill;
            panelControl2.Controls.Add(list);
        }

        private void btnInsertCategory_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textEdit1.Text))
            {
                Category category = new Category();
                string tentl = textEdit1.Text;
                DateTime date = DateTime.Now;
                category.Name = tentl;
                category.Date = date;
                CategoryBUS cate = new CategoryBUS();
                if (cate.KiemTraTonTai(category.Name))
                    MessageBox.Show("Đã có thể loại này rồi!!!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    if (cate.ThemTheLoai(category) > 0)
                    {
                        LoadData();
                        textEdit1.Controls.Clear();
                    }
                    else
                        MessageBox.Show("Thêm mới không thành công, đã có lỗi xảy ra.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
                MessageBox.Show("Không được để trống tên thể loại", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Category category = new Category();
            CategoryBUS cate = new CategoryBUS();
            cate.XoaTheLoai(category);
            list.Returnten();
        }

    }
}
