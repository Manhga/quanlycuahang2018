﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using DTO;
using BUS;

namespace QuanLyCuaHang2018
{
    public partial class Login : DevExpress.XtraEditors.XtraForm
    {
        public Login()
        {
            InitializeComponent();
        }

        public void skins()
        {
            DevExpress.LookAndFeel.DefaultLookAndFeel themes = new DevExpress.LookAndFeel.DefaultLookAndFeel();
            themes.LookAndFeel.SkinName = "Valentine";
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            skins();
        }

        private void btnThoat_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool MLogin(String code_employee, String password)
        {

            return false;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            onclickLogin();
            this.Close();
        }

        public void onclickLogin()
        {
            StringBuilder password2 = new StringBuilder();
            int code_employee = int.Parse(txtUser.Text);
            String password = txtPassword.Text;
            byte[] temp = Encoding.UTF8.GetBytes(password);
            User tk = new User();
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] passwordnew = md5.ComputeHash(temp);
            for (int i = 0; i < passwordnew.Length; i++)
            {
                password2.Append(passwordnew[i].ToString("x2"));
            }
            String pass = password2.ToString();
            int check = managerBUS.Instance.CheckLogin(code_employee, pass, out tk);
            if(check == -1)
            {
                MessageBox.Show("Tai Khoan khong ton tai.", "Loi!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if(check == 0)
            {
                MessageBox.Show("Sai Mat khau, vui long kiem tra lai.", "Loi!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if(tk.role_id == 2)
                {
                    frmMain f = new frmMain();
                    this.Hide();
                    f.ShowDialog();
                }
            }
        }

        
    }
}
