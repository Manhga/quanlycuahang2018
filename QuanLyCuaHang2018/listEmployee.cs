﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Controls;

namespace QuanLyCuaHang2018
{
    public partial class listEmployee : DevExpress.XtraEditors.XtraUserControl
    {
        public listEmployee()
        {
            InitializeComponent();
        }

        public object mahang;
        private void listEmployee_Load(object sender, EventArgs e)
        {
            grdListEmployee.DataSource = managerBUS.Instance.ListEmployee();
        }

        private void grdListEmployee_Click(object sender, EventArgs e)
        {
            int row_id = gvListEmployee.FocusedRowHandle;
            String cold_name = "manv";
            mahang = gvListEmployee.GetRowCellValue(row_id, cold_name);
            String ma = mahang.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            User user = new User();
            int row_id = gvListEmployee.FocusedRowHandle;
            String cold_name = "manv";
            object mahang = gvListEmployee.GetRowCellValue(row_id, cold_name);
            user.manv = int.Parse(mahang.ToString());
            managerBUS tk = new managerBUS();
            if (tk.DelEmployee(user) > 0)
            {
                grdListEmployee.DataSource = managerBUS.Instance.ListEmployee();
            }
            else
                MessageBox.Show("Thêm mới không thành công, đã có lỗi xảy ra.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

    }
}
