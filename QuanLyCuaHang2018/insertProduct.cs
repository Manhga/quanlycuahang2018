﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;

namespace QuanLyCuaHang2018
{
    public partial class insertProduct : DevExpress.XtraEditors.XtraUserControl
    {
        public insertProduct()
        {
            InitializeComponent();
        }

        private void insertProduct_Load(object sender, EventArgs e)
        {
            listProduct list = new listProduct();
            list.Dock = DockStyle.Fill;
            pacList.Controls.Add(list);
            CategoryBUS category = new CategoryBUS();
            DataTable data = category.category_load();
            for(int i = 0; i < data.Rows.Count; i++)
            {
                cboCategory.Properties.Items.Add(data.Rows[i][1]);
            }
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {

        }
    }
}
