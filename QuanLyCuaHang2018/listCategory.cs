﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using BUS;
using DTO;
namespace QuanLyCuaHang2018
{
    public partial class listCategory : DevExpress.XtraEditors.XtraUserControl
    {
        public object matl;
        public listCategory()
        {
            InitializeComponent();
        }

        private void listCategory_Load(object sender, EventArgs e)
        {
            CategoryBUS cate = new CategoryBUS();
            gridControl1.DataSource = cate.cate_Load();
        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        public void Returnten()
        {
            int id_row = gvListcategory.FocusedRowHandle;
            XtraMessageBox.Show("id: " + gvListcategory.FocusedRowHandle);
            Object ten = gvListcategory.GetRowCellDisplayText(id_row, "category_name");
            MessageBox.Show(ten.ToString());
        }
    }
}
