﻿namespace QuanLyCuaHang2018
{
    partial class listCategory
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gvListcategory = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grdcName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdcDate_Create = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListcategory)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(837, 514);
            this.panelControl1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gvListcategory;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(833, 510);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvListcategory});
            this.gridControl1.Click += new System.EventHandler(this.gridControl1_Click);
            // 
            // gvListcategory
            // 
            this.gvListcategory.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grdcName,
            this.grdcDate_Create});
            this.gvListcategory.GridControl = this.gridControl1;
            this.gvListcategory.Name = "gvListcategory";
            // 
            // grdcName
            // 
            this.grdcName.Caption = "Tên thể loại";
            this.grdcName.FieldName = "category_name";
            this.grdcName.Name = "grdcName";
            this.grdcName.Visible = true;
            this.grdcName.VisibleIndex = 0;
            // 
            // grdcDate_Create
            // 
            this.grdcDate_Create.Caption = "Ngày tạo";
            this.grdcDate_Create.FieldName = "created_at";
            this.grdcDate_Create.Name = "grdcDate_Create";
            this.grdcDate_Create.Visible = true;
            this.grdcDate_Create.VisibleIndex = 1;
            // 
            // listCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl1);
            this.Name = "listCategory";
            this.Size = new System.Drawing.Size(837, 514);
            this.Load += new System.EventHandler(this.listCategory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListcategory)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gvListcategory;
        private DevExpress.XtraGrid.Columns.GridColumn grdcName;
        private DevExpress.XtraGrid.Columns.GridColumn grdcDate_Create;
    }
}
