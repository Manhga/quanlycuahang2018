﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Controls;
namespace QuanLyCuaHang2018
{
    public partial class editCategory : DevExpress.XtraEditors.XtraUserControl
    {
        public editCategory()
        {
            InitializeComponent();
        }

        private void editCategory_Load(object sender, EventArgs e)
        {
            CategoryBUS cate = new CategoryBUS();
            gridControl1.DataSource = cate.cate_Load();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Category category = new Category();
            int row_id = gvListcategory.FocusedRowHandle;
            String cold_name = "category_name";
            object mahang = gvListcategory.GetRowCellValue(row_id, cold_name);
            category.Name = mahang.ToString();
            CategoryBUS cate = new CategoryBUS();
            if (cate.XoaTheLoai(category) > 0)
            {
                gridControl1.DataSource = cate.cate_Load();
            }
            else
                MessageBox.Show("Thêm mới không thành công, đã có lỗi xảy ra.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Category category = new Category();
            int row_id = gvListcategory.FocusedRowHandle;
            String cold_name = "category_name";
            String cold_name2 = "category_id";
            object tentheloai = gvListcategory.GetRowCellValue(row_id, cold_name);
            object matheloai = gvListcategory.GetRowCellValue(row_id, cold_name2);
            category.Name = tentheloai.ToString();
            category.Id = int.Parse(matheloai.ToString());
            CategoryBUS cate = new CategoryBUS();
            if (cate.SuaTheLoai(category) > 0)
            {
                gridControl1.DataSource = cate.cate_Load();
            }
            else
            {
                MessageBox.Show("Đã xảy ra lỗi hoặc bạn không có quyền sửa trường này.", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                gridControl1.DataSource = cate.cate_Load();
            }
        }
    }
}
