﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;

namespace QuanLyCuaHang2018
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void btnListEmployee_ItemClick(object sender, ItemClickEventArgs e)
        {
            paCHienThi.Controls.Clear();
            listEmployee list = new listEmployee();
            list.Dock = System.Windows.Forms.DockStyle.Fill;
            paCHienThi.Controls.Add(list);
        }

        private void btnAddEmployee_ItemClick(object sender, ItemClickEventArgs e)
        {
            paCHienThi.Controls.Clear();
            registEmployee re = new registEmployee();
            re.Dock = System.Windows.Forms.DockStyle.Fill;
            paCHienThi.Controls.Add(re);
        }

        private void btnListProduct_ItemClick(object sender, ItemClickEventArgs e)
        {
            paCHienThi.Controls.Clear();
            listProduct list = new listProduct();
            list.Dock = System.Windows.Forms.DockStyle.Fill;
            paCHienThi.Controls.Add(list);
        }

        private void btnInsertProduct_ItemClick(object sender, ItemClickEventArgs e)
        {
            paCHienThi.Controls.Clear();
            insertProduct insert = new insertProduct();
            insert.Dock =DockStyle.Fill;
            paCHienThi.Controls.Add(insert);
        }

        private void btnListCategory_ItemClick(object sender, ItemClickEventArgs e)
        {
            paCHienThi.Controls.Clear();
            editCategory list = new editCategory();
            list.Dock = DockStyle.Fill;
            paCHienThi.Controls.Add(list);
        }

        private void btnInsertCategory_ItemClick(object sender, ItemClickEventArgs e)
        {
            paCHienThi.Controls.Clear();
            insertCategory insert = new insertCategory();
            insert.Dock = DockStyle.Fill;
            paCHienThi.Controls.Add(insert);
        }
    }
}