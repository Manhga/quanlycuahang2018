﻿namespace QuanLyCuaHang2018
{
    partial class insertProduct
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnDeleteProduct = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditProduct = new DevExpress.XtraEditors.SimpleButton();
            this.btnInsert = new DevExpress.XtraEditors.SimpleButton();
            this.txtCost = new DevExpress.XtraEditors.TextEdit();
            this.txtTotal_Add = new DevExpress.XtraEditors.TextEdit();
            this.txtNameProduct = new DevExpress.XtraEditors.TextEdit();
            this.lblCost = new DevExpress.XtraEditors.LabelControl();
            this.lblTotal_Add = new DevExpress.XtraEditors.LabelControl();
            this.lblCategory = new DevExpress.XtraEditors.LabelControl();
            this.lblName = new DevExpress.XtraEditors.LabelControl();
            this.pacList = new DevExpress.XtraEditors.PanelControl();
            this.cboCategory = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal_Add.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pacList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategory.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnDeleteProduct);
            this.panelControl1.Controls.Add(this.btnEditProduct);
            this.panelControl1.Controls.Add(this.btnInsert);
            this.panelControl1.Controls.Add(this.cboCategory);
            this.panelControl1.Controls.Add(this.txtCost);
            this.panelControl1.Controls.Add(this.txtTotal_Add);
            this.panelControl1.Controls.Add(this.txtNameProduct);
            this.panelControl1.Controls.Add(this.lblCost);
            this.panelControl1.Controls.Add(this.lblTotal_Add);
            this.panelControl1.Controls.Add(this.lblCategory);
            this.panelControl1.Controls.Add(this.lblName);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(791, 165);
            this.panelControl1.TabIndex = 0;
            // 
            // btnDeleteProduct
            // 
            this.btnDeleteProduct.Location = new System.Drawing.Point(683, 117);
            this.btnDeleteProduct.Name = "btnDeleteProduct";
            this.btnDeleteProduct.Size = new System.Drawing.Size(85, 33);
            this.btnDeleteProduct.TabIndex = 25;
            this.btnDeleteProduct.Text = "Xóa hàng hóa";
            // 
            // btnEditProduct
            // 
            this.btnEditProduct.Location = new System.Drawing.Point(683, 68);
            this.btnEditProduct.Name = "btnEditProduct";
            this.btnEditProduct.Size = new System.Drawing.Size(85, 33);
            this.btnEditProduct.TabIndex = 26;
            this.btnEditProduct.Text = "Sửa hàng hóa";
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(683, 15);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(85, 33);
            this.btnInsert.TabIndex = 27;
            this.btnInsert.Text = "Thêm hàng hóa";
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // txtCost
            // 
            this.txtCost.Location = new System.Drawing.Point(460, 121);
            this.txtCost.Name = "txtCost";
            this.txtCost.Size = new System.Drawing.Size(206, 20);
            this.txtCost.TabIndex = 22;
            // 
            // txtTotal_Add
            // 
            this.txtTotal_Add.Location = new System.Drawing.Point(460, 29);
            this.txtTotal_Add.Name = "txtTotal_Add";
            this.txtTotal_Add.Size = new System.Drawing.Size(206, 20);
            this.txtTotal_Add.TabIndex = 23;
            // 
            // txtNameProduct
            // 
            this.txtNameProduct.Location = new System.Drawing.Point(122, 28);
            this.txtNameProduct.Name = "txtNameProduct";
            this.txtNameProduct.Size = new System.Drawing.Size(192, 20);
            this.txtNameProduct.TabIndex = 21;
            // 
            // lblCost
            // 
            this.lblCost.Location = new System.Drawing.Point(382, 121);
            this.lblCost.Name = "lblCost";
            this.lblCost.Size = new System.Drawing.Size(41, 13);
            this.lblCost.TabIndex = 18;
            this.lblCost.Text = "Đơn giá:";
            // 
            // lblTotal_Add
            // 
            this.lblTotal_Add.Location = new System.Drawing.Point(377, 35);
            this.lblTotal_Add.Name = "lblTotal_Add";
            this.lblTotal_Add.Size = new System.Drawing.Size(46, 13);
            this.lblTotal_Add.TabIndex = 19;
            this.lblTotal_Add.Text = "Số lượng:";
            // 
            // lblCategory
            // 
            this.lblCategory.Location = new System.Drawing.Point(52, 121);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(41, 13);
            this.lblCategory.TabIndex = 20;
            this.lblCategory.Text = "Thể loại:";
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(23, 35);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(70, 13);
            this.lblName.TabIndex = 17;
            this.lblName.Text = "Tên hàng hóa:";
            // 
            // pacList
            // 
            this.pacList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pacList.Location = new System.Drawing.Point(0, 165);
            this.pacList.Name = "pacList";
            this.pacList.Size = new System.Drawing.Size(791, 349);
            this.pacList.TabIndex = 1;
            // 
            // cboCategory
            // 
            this.cboCategory.Location = new System.Drawing.Point(122, 121);
            this.cboCategory.Name = "cboCategory";
            this.cboCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCategory.Size = new System.Drawing.Size(192, 20);
            this.cboCategory.TabIndex = 24;
            // 
            // insertProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pacList);
            this.Controls.Add(this.panelControl1);
            this.Name = "insertProduct";
            this.Size = new System.Drawing.Size(791, 514);
            this.Load += new System.EventHandler(this.insertProduct_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal_Add.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pacList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategory.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl pacList;
        private DevExpress.XtraEditors.SimpleButton btnDeleteProduct;
        private DevExpress.XtraEditors.SimpleButton btnEditProduct;
        private DevExpress.XtraEditors.SimpleButton btnInsert;
        private DevExpress.XtraEditors.TextEdit txtCost;
        private DevExpress.XtraEditors.TextEdit txtTotal_Add;
        private DevExpress.XtraEditors.TextEdit txtNameProduct;
        private DevExpress.XtraEditors.LabelControl lblCost;
        private DevExpress.XtraEditors.LabelControl lblTotal_Add;
        private DevExpress.XtraEditors.LabelControl lblCategory;
        private DevExpress.XtraEditors.LabelControl lblName;
        private DevExpress.XtraEditors.ComboBoxEdit cboCategory;
    }
}
