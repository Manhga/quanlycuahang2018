﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Mask;
using System.Security.Cryptography;
using BUS;
using DTO;
namespace QuanLyCuaHang2018
{
    public partial class registEmployee : DevExpress.XtraEditors.XtraUserControl
    {
        public registEmployee()
        {
            InitializeComponent();
        }

        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            User user = new User();
            StringBuilder password2 = new StringBuilder();
            try
            {
                String ma = "2018" + txtCodeEmployee.Text;
                user.manv = int.Parse(ma);
            }
            catch
            {
                MessageBox.Show("bạn phải nhập đủ các trường", "Cảnh báo");
            } 
            String name = txtName.Text;
            int phone = int.Parse(txtPhone.Text);
            //DateTime Birthday = DateTime.Parse((DateTime.Parse(dtBirthDay.EditValue.ToString())).ToShortDateString());
            user.email = txtEmail.Text;
            if(rdoMale.Checked)
            {
                user.sex = "Nam";
            }
            else if(rdoFeMale.Checked)
            {
                user.sex = "Nu";
            }
            else
            {
                MessageBox.Show("Bạn chưa chọn giới tính", "Cảnh báo");
            }

            if (rdoManager.Checked)
            {
               user.role_id = 2;
            }
            else if (rdoEmployee.Checked)
            {
               user.role_id = 3;
            }
            String password = "12345678";
            byte[] temp = Encoding.UTF8.GetBytes(password);
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] passwordnew = md5.ComputeHash(temp);
            for (int i = 0; i < passwordnew.Length; i++)
            {
                password2.Append(passwordnew[i].ToString("x2"));
            }
            user.password = password2.ToString();
            user.name = name;
            user.phone = phone;
            managerBUS manager = new managerBUS();           
            if (manager.AddEmployee(user) > 0)
            {
                MessageBox.Show("Bạn đã thêm thành công");
            }
            else
                MessageBox.Show("Thêm mới không thành công, đã có lỗi xảy ra.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void registEmployee_Load(object sender, EventArgs e)
        {
        }

    }
}
